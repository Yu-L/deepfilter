source ./config.sh

# Change to your OUTPUT_BUCKET from earlier.
OUTPUT_BUCKET="${OUTPUT_GCS_BUCKET}/customized_training"
TRAINING_DIR="${OUTPUT_BUCKET}/training_dir"
sudo nvidia-docker run -p 0.0.0.0:80:80 \
    -v /root:/root \
    "${GPU_DEEPVARIANT}" \
    tensorboard --logdir ${TRAINING_DIR}/eval --port=80 
