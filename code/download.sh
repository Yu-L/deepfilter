# download BGISEQ500 WGS sequence data
ascp -v -k 1 -QT -l 200M -i /home/deepvariant/.aspera/connect/etc/asperaweb_id_dsa.openssh \
anonftp@ftp-trace.ncbi.nlm.nih.gov:/giab/ftp/data/NA12878/BGISEQ500/standard_library/ERR1799697_1.fastq.gz ./

ascp -v -k 1 -QT -l 200M -i /home/deepvariant/.aspera/connect/etc/asperaweb_id_dsa.openssh \
anonftp@ftp-trace.ncbi.nlm.nih.gov:/giab/ftp/data/NA12878/BGISEQ500/standard_library/ERR1799697_2.fastq.gz ./

ascp -v -k 1 -QT -l 200M -i /home/deepvariant/.aspera/connect/etc/asperaweb_id_dsa.openssh \
anonftp@ftp-trace.ncbi.nlm.nih.gov:/giab/ftp/data/NA12878/BGISEQ500/standard_library/ERR1799698_1.fastq.gz ./

ascp -v -k 1 -QT -l 200M -i /home/deepvariant/.aspera/connect/etc/asperaweb_id_dsa.openssh \
anonftp@ftp-trace.ncbi.nlm.nih.gov:/giab/ftp/data/NA12878/BGISEQ500/standard_library/ERR1799698_2.fastq.gz ./
