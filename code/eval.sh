
#Set the variables:
source ./config.sh

##Start model_train and model_eval

# #model_eval on GPUs:
time sudo nvidia-docker run \
   -v /root:/root \
   "${GPU_DEEPVARIANT}" \
   /opt/deepvariant/bin/model_eval \
   --dataset_config_pbtxt="${OUTPUT_BUCKET}/validation_set.dataset_config.pbtxt" \
   --checkpoint_dir="${TRAINING_DIR}" \
   --batch_size=512 > "${LOG_DIR}/eval.log" 2>&1 &
