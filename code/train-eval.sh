
#Set the variables:
source ./config.sh

##Start model_train and model_eval

( time sudo nvidia-docker run \
  -v /root:/root \
  "${GPU_DEEPVARIANT}" \
  /opt/deepvariant/bin/model_train \
  --dataset_config_pbtxt="${OUTPUT_BUCKET}/training_set.dataset_config.pbtxt" \
  --train_dir="${TRAINING_DIR}" \
  --model_name="inception_v3" \
  --number_of_steps=50000 \
  --save_interval_secs=300 \
  --batch_size=512 \
  --learning_rate=0.008 \
  --start_from_checkpoint="${GCS_PRETRAINED_WGS_MODEL}" \
) > "${LOG_DIR}/train.log" 2>&1 &

# #model_eval on CPUs:
 sudo docker run \
   -v /root:/root \
   "${DEEPVARIANT}" \
   /opt/deepvariant/bin/model_eval \
   --dataset_config_pbtxt="${OUTPUT_BUCKET}/validation_set.dataset_config.pbtxt" \
   --checkpoint_dir="${TRAINING_DIR}" \
   --batch_size=512 > "${LOG_DIR}/eval.log" 2>&1 &
