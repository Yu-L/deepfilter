
#Set the variables:

source activate apache-beam

YOUR_PROJECT=REPLACE_WITH_YOUR_PROJECT

GPU_DEEPVARIANT="registry.cn-shanghai.aliyuncs.com/liu-yu/deepvariant:0.8.0"
DEEPVARIANT="luweireed/deepfilter:v2.0"

MODEL_BUCKET="/root/deepvariant/source/model"
GCS_PRETRAINED_WGS_MODEL="${MODEL_BUCKET}/model.ckpt"

OUTPUT_GCS_BUCKET="/root/localdisk/root/OUTPUT_GCS_BUCKET"
OUTPUT_BUCKET="${OUTPUT_GCS_BUCKET}/customized_training"
TRAINING_DIR="${OUTPUT_BUCKET}/training_dir"

BASE="/root/deepvariant/pipeline/train_BGISEQ-500"
DATA_BUCKET=gs://deepvariant/training-case-study/BGISEQ-HG001

INPUT_DIR="${BASE}/input"
BIN_DIR="${INPUT_DIR}/bin"
DATA_DIR="${INPUT_DIR}/data"
OUTPUT_DIR="${BASE}/output"
LOG_DIR="${OUTPUT_DIR}/logs"
SHUFFLE_SCRIPT_DIR="/root/deepvariant/source/deepvariant/tools"

REF="${DATA_DIR}/GRCh38.genome.fa"
BAM="${DATA_DIR}/NA12878.rmdup.sort.bam"
TRUTH_VCF="${DATA_DIR}/HG001_GRCh38_GIAB_highconf_CG-IllFB-IllGATKHC-Ion-10X-SOLID_CHROM1-X_v.3.3.2_highconf_PGandRTGphasetransfer.vcf.gz"
TRUTH_BED="${DATA_DIR}/HG001_GRCh38_GIAB_highconf_CG-IllFB-IllGATKHC-Ion-10X-SOLID_CHROM1-X_v.3.3.2_highconf_nosomaticdel_noCENorHET7.bed"

N_SHARDS="8"

#mkdir 
mkdir -p "${OUTPUT_DIR}"
mkdir -p "${BIN_DIR}"
# mkdir -p "${DATA_DIR}"
mkdir -p "${LOG_DIR}"
mkdir -p "${TRAINING_DIR}"