
## Pick a model

#Set the variables:

source ./config.sh

#Copy the *.metrics file to tmp path

#mkdir -p /tmp/metrics
#cp  ${TRAINING_DIR}/*metrics /tmp/metrics/

python ${SHUFFLE_SCRIPT_DIR}/print_f1.py \
--metrics_dir="/tmp/metrics/" | sort -k 3 -g -r | head -1
