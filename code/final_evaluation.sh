

source ./config.sh

# do the final evaluation on the test set
 time sudo nvidia-docker run \
    -v /root:/root \
    "${GPU_DEEPVARIANT}" \
    /opt/deepvariant/bin/call_variants \
    --outfile "${OUTPUT_DIR}/test_set.cvo.tfrecord.gz" \
    --examples "${OUTPUT_DIR}/test_set.no_label.tfrecord@${N_SHARDS}.gz" \
    --checkpoint "${TRAINING_DIR}/model.ckpt-34518" \
 >"${LOG_DIR}/test_set.call_variants.log" 2>&1

# run postprocess_variants to generate the final callsets in VCF format

 time sudo nvidia-docker run \
    -v /root:/root \
    "${GPU_DEEPVARIANT}" \
    /opt/deepvariant/bin/postprocess_variants \
    --ref "${REF}" \
    --infile "${OUTPUT_DIR}/test_set.cvo.tfrecord.gz" \
    --outfile "${OUTPUT_DIR}/test_set.vcf.gz" \
 >"${LOG_DIR}/test_set.postprocess_variants.log" 2>&1 

 #run hap.py to complete the evaluation on chromosome 20:
## tabix -p vcf "${OUTPUT_DIR}/test_set.vcf.gz"
 time sudo docker run -it \
-v "${DATA_DIR}:${DATA_DIR}" \
-v "${OUTPUT_DIR}:${OUTPUT_DIR}" \
pkrusche/hap.py /opt/hap.py/bin/hap.py \
  "${TRUTH_VCF}" \
  "${OUTPUT_DIR}/test_set.vcf.gz" \
  -f "${TRUTH_BED}" \
  -r "${REF}" \
  -o "${OUTPUT_DIR}/chr20-calling.happy.output" \
  -l chr20 \
  --engine=vcfeval

## The baseline we're comparing to is to directly use the WGS model to make the calls, using this command:

sudo nvidia-docker run \
-v "${DATA_DIR}:${DATA_DIR}" \
-v "${OUTPUT_DIR}:${OUTPUT_DIR}" \
"${GPU_DEEPVARIANT}" \
/opt/deepvariant/bin/run_deepvariant \
--model_type=WGS \
--ref="${REF}" \
--reads="${BAM}" \
--output_vcf="${OUTPUT_DIR}/baseline.vcf.gz" \
--num_shards=${N_SHARDS}

 #run hap.py to complete the evaluation on chromosome 20:
##tabix -p vcf "${OUTPUT_DIR}/baseline.vcf.gz"

time sudo docker run -it \
-v "${DATA_DIR}:${DATA_DIR}" \
-v "${OUTPUT_DIR}:${OUTPUT_DIR}" \
pkrusche/hap.py /opt/hap.py/bin/hap.py \
  "${TRUTH_VCF}" \
  "${OUTPUT_DIR}/baseline.vcf.gz" \
  -f "${TRUTH_BED}" \
  -r "${REF}" \
  -o "${OUTPUT_DIR}/baseline-calling.happy.output" \
  -l chr20 \
  --engine=vcfeval
