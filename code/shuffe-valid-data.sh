#Set the variables:
source ./config.sh

##Shuffle each set of examples and generate a data configuration file for each

#shuffle Validation set locally using DirectRunner

# First, clean up existing files.
rm -f "${OUTPUT_DIR}/validation_set.with_label.shuffled-?????-of-?????.tfrecord.gz"
rm -f "${OUTPUT_DIR}/validation_set.dataset_config.pbtxt"

time python ${SHUFFLE_SCRIPT_DIR}/shuffle_tfrecords_beam.py \
  --input_pattern_list="${OUTPUT_DIR}/validation_set.with_label.tfrecord-?????-of-00008.gz" \
  --output_pattern_prefix="${OUTPUT_DIR}/validation_set.with_label.shuffled" \
  --output_dataset_config_pbtxt="${OUTPUT_DIR}/validation_set.dataset_config.pbtxt" \
  --output_dataset_name="HG001" \
  --runner=DirectRunner

##
#   Output is in ${OUTPUT_DIR}/validation_set.with_label.shuffled-00000-of-00001.tfrecord.gz
#   Data config file is in ${OUTPUT_DIR}/validation_set.dataset_config.pbtxt.
##

